//
// Author:		Jedd Haberstro
// Repository:	https://bitbucket.org/jhaberstro/cs4-project/overview
//				git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#include "CrossoutGame.h"
#include "Parsing.h"
#include <iostream>
#include <sstream>
#include <vector>
#include <cstring>
#include <limits>

CrossoutGame::CrossoutGame() {
}

bool CrossoutGame::initialise(int argc, char* argv[]) {
    GameMode gameMode;
    std::vector< int > initialBoard;
    bool failure = false;

    char** numArgsStart = 0;
    if (argc == 2) {
        gameMode = GAMEMODE_NORMAL;
        numArgsStart = argv;
    }
    else if (argc == 3 && strcmp(argv[0], "play") == 0) {
        gameMode = GAMEMODE_INTERACTIVE;
        numArgsStart = argv + 1;
    }
    else {
        failure = true;
    }

    int maxNum = -1;
    int maxSum = -1;
    if (!failure) {
        if (std::parse(numArgsStart[0], maxNum) && std::parse(numArgsStart[1], maxSum)) {
            if (maxNum <= 0 || maxSum <= 0) {
                std::cerr << "max_num and/or max_sum have invalid values." << std::endl;
                failure = true;
            }
            else {
                for (int i = 1; i <= maxNum; ++i) {
                    initialBoard.push_back(i);
                }
            }
        }
        else {
            failure = true;
        }
    }

    if (failure) {
        std::cerr << "usage:\tcrossout [play] max_num max_sum" << std::endl;
    }
    else {
        state_.initialise(gameMode, initialBoard, maxNum, maxSum);
    }

    return failure == false;
}


bool CrossoutGame::update() {
    switch(state_.getGameMode()) {
        case GAMEMODE_NORMAL: {
            playNormal();
            return false;
        }

        case GAMEMODE_INTERACTIVE: {
            return playInteractive();
        }
    }

    return true;
}

void CrossoutGame::end() {
   if (state_.getGameMode() == GAMEMODE_INTERACTIVE) {
        std::cout << "No more possible moves left!" << std::endl;
        if (state_.isHumansTurn()) {
            std::cout << "You have won the game!" << std::endl;
        } 
        else {
            std::cout << "The Computer won!" << std::endl;
        }
   } 
}

bool CrossoutGame::playInteractive() {
    if (state_.isHumansTurn()) {
        std::cout << "Human" << std::endl << "-------------------" << std::endl;
        std::cout << "The current board:" << std::endl << "   " << state_.getCurrentPosition() << std::endl; 

        std::vector< int > choices;
        bool invalidChoice = true;
        while (invalidChoice) {
            choices.clear();
            std::cout << "Please enter the numbers which you wish to crossout: ";

            // Get a whole line
            std::string line = "";
            int numNumbers = 1;
            char c;
            while (std::cin.get(c)) {
                if (c == '\n') {
                    break;
                }
                else if (c == ' ') {
                    numNumbers += 1;
                }
                line += c;
            }
            
            if (!std::cin) {
                invalidInputRefresh();
                continue;
            }
            else if (numNumbers <= 2) {
                // parse our line
                bool parsedCorrectly = true;
                std::istringstream iss(line);
                int sum = 0;
                for (int i = 0; i < numNumbers; ++i) {
                    int n;
                    if (iss >> n) {
                        choices.push_back(n);
                        sum += n;
                    }
                    else {
                        parsedCorrectly = false;
                        break;
                    }
                }

                if (!parsedCorrectly) {
                    std::cout << "Invalid input. Please reenter your choices." << std::endl;
                    continue;
                }

                // validate the chosen numbers
                if (sum > state_.getMaxSum()) {
                    std::cout << "The sum of your choices is too large." << std::endl;
                }
                else {
                    bool validated = true;
                    std::vector< int >& board = state_.getCurrentPosition().getBoard();
                    for (int choice = 0; choice < choices.size(); ++choice) {
                        bool found = false;
                        for (int i = 0; i < board.size(); ++i) {
                            if (board[i] == choices[choice]) {
                                found = true;
                                break;
                            }
                        }

                        if (!found) {
                            std::cout << choices[choice] << " is not a valid number. Please reenter your choices." << std::endl;
                            validated = false;
                            break;;
                        }
                    }

                    invalidChoice = !validated;
                }
            }
            else if (numNumbers >= 2) {
                std::cout << "You may only crossout 1 or 2 numbers." << std::endl;
            }
        }

        std::vector< int >& board = state_.getCurrentPosition().getBoard();
        for (int i = 0; i < choices.size(); ++i) {
            for (int j = 0; j < board.size(); ++j) {
                if (choices[i] == board[j]) {
                    board.erase(board.begin() + j);
                    break;
                }
            }
        }
    }
    else {
        std::cout << "Computer" << std::endl << "-------------------" << std::endl;
        std::cout << "The current board:" << std::endl << "   " << state_.getCurrentPosition() << std::endl; 

        CrossoutPosition result = state_.evaluate(state_.getCurrentPosition());
        std::cout << "Board after computer:" << std::endl << "   " << result << std::endl;
        state_.getCurrentPosition() = result;
    }

    if (state_.isFinal(state_.getCurrentPosition())) {
        return false;
    }
    else {
        state_.setHumansTurn(!state_.isHumansTurn());
    }

    std::cout << std::endl;
    return true;
}

void CrossoutGame::playNormal() {
    CrossoutPosition position(state_.getInitialBoard());
    CrossoutPosition result = state_.evaluate(position);
    std::cout << "Original board:\t" << position << std::endl;
    std::cout << "New board:\t" << result << std::endl;
    if (!result.isWin()) {
        std::cout << "The resulting move is a win." << std::endl;
    }
    else {
        std::cout <<  "The resulting move is a lose." << std::endl;
    }
}

