//
// Author:	    Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef CROSSOUTGAME_H
#define CROSSOUTGAME_H

#include "Game.h"
#include "StateController.h"
#include "CrossoutState.h"

/// Game implementation for crossout
/// @author Jedd Haberstro
class CrossoutGame : public Game
{
public:

    /// Default constructor
    CrossoutGame();
    
	/// See Game::initialise
	virtual bool initialise(int argc, char* argv[]);

    /// See Game::update
    virtual bool update();

	/// See Game::end
    virtual void end();

private:
	
	/// PlayNormal is responsible for the game logic to be performed in update
	/// when the game is being played in Normal mode
	virtual void playNormal();

	/// PlayInteractive is responsible for the game logic to be performed in
	/// update when the game is being played in Interactive mode (human vs
	/// computer).
	virtual bool playInteractive();

private:

    StateController< CrossoutState > state_;
};

#endif // CROSSOUTGAME_H
