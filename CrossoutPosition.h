
// Author:      Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef CROSSOUT_POSITION_H
#define CROSSOUT_POSITION_H

#include <vector>
#include <iostream>

/// CrossoutPosition represents a single position in the game of Crossout.
/// @author Jedd Haberstro
class CrossoutPosition
{
    friend class CrossoutState;

    friend std::ostream& operator << (std::ostream& os, CrossoutPosition const& position);

public:

    /// Constructor
    /// @param board the board state
    CrossoutPosition(std::vector< int > const& board);

    /// Copy constructor
    /// @param other the object which to copy
    CrossoutPosition(CrossoutPosition const& other);

    /// Returns the vector representing the board associated with this position
    /// @returns the board
    std::vector< int >& getBoard();

    /// Returns the vector representing the board associated with this position
    /// @returns the board
    std::vector< int > const& getBoard() const;

    /// negates the value of use in Evaluate
    void negate();

    /// Reports whether this is a winning move.
    /// @returns true if the move is a winning move, else false
    bool isWin() const; 

    /// Score is simply a win or lose. Set the score to win.
    void incrementScore(); 

    /// Score is simply a win or lose. Set the score to lose..
    void decrementScore(); 

    /// Sets the score to the appropriate value for a final position
    void setScoreForFinalPosition();
    
    /// Compares two positions
    /// @param other position to compare against
    bool isWorthMore(CrossoutPosition const& other) const;

    /// Compares two positions
    /// @param other position to compare against
    bool operator< (CrossoutPosition const& other) const;

protected:

    CrossoutPosition();

private:

    std::vector< int > board;
    bool score;
};


inline CrossoutPosition::CrossoutPosition(std::vector< int > const& board)
: board(board), score(false) {
}

inline CrossoutPosition::CrossoutPosition(CrossoutPosition const& other)
: board(other.board), score(other.score) {
}

inline CrossoutPosition::CrossoutPosition()
: score(false) {
}

inline std::vector< int >& CrossoutPosition::getBoard() {
    return board;
}

inline std::vector< int > const& CrossoutPosition::getBoard() const {
    return board;
}

inline void CrossoutPosition::negate() {
}

inline bool CrossoutPosition::isWin() const {
    return score;
}

inline void CrossoutPosition::incrementScore() {
    score = true;
}

inline void CrossoutPosition::decrementScore() {
    score = false;
}

inline void CrossoutPosition::setScoreForFinalPosition() {
    score = false;
}

inline bool CrossoutPosition::isWorthMore(CrossoutPosition const& other) const {
    return !score;
}

inline bool CrossoutPosition::operator< (CrossoutPosition const& other) const {
    return board < other.board;
}

inline std::ostream& operator << (std::ostream& os, CrossoutPosition const& position) {
    for (int i = 0; i < position.board.size(); ++i) {
        os << position.board[i] << " ";
    }
    
    return os;
}

#endif // CROSSOUT_POSITION_H
