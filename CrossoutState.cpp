// 
// Author:      Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#include "CrossoutState.h"


void CrossoutState::initialise(GameMode mode, std::vector< int > const& initialBoard, int maxNum, int maxSum) {
    this->initialBoard = initialBoard;
    gameMode = mode;
    this->maxNum = maxNum;
    this->maxSum = maxSum;
    humansTurn = true;
    currentPosition = Position(initialBoard);
}

bool CrossoutState::isFinal(CrossoutPosition const& position) const {
    std::vector< int > const& board = position.getBoard();
    for (int i = 0; i < board.size(); ++i) {
        if (board[i] <= maxSum) {
            return false;
        }
        
        for (int j = i + 1; j < board.size(); ++j) {
            int sum = board[i] + board[j];
            if (sum <= maxSum) {
                return false;
            }
        }
    }

    return true;
}

std::vector< CrossoutPosition > CrossoutState::legalPositions(CrossoutPosition const& position) const {
    std::vector< CrossoutPosition > result;
    std::vector< int > board = position.getBoard();
    for (int i = 0; i < board.size(); ++i) {
        if (board[i] <= maxSum) {
            std::vector< int > newBoard(board);
            newBoard.erase(newBoard.begin() + i);
            result.push_back(CrossoutPosition(newBoard));
        }

        for (int j = i + 1; j < board.size(); ++j) {
            int sum = board[i] + board[j];
            if (sum <= maxSum) {
                std::vector< int > newBoard(board);
                newBoard.erase(newBoard.begin() + j);
                newBoard.erase(newBoard.begin() + i);
                result.push_back(CrossoutPosition(newBoard));
            }
        }
    }

    return result;
}
