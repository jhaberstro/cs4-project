// 
// Author:      Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef CROSSOUT_STATE_H
#define CROSSOUT_STATE_H

#include "CrossoutPosition.h"
#include "Game.h"

/// CrossoutState represents all of the state for the game crossout
/// @author Jedd Haberstro
class CrossoutState
{
protected:

    typedef CrossoutPosition Position;

public:


    /// Initialises all of the state
    /// @param mode which mode the game will be played in
    /// @param initialPiles the piles of pebbles the game is launched with.
    void initialise(GameMode mode, std::vector< int > const& board, int maxNum, int maxSum);

    /// Gets the game mode
    /// @returns the mode
    GameMode getGameMode() const;

    /// Gets the initial piles the game is launched with
    /// @returns vector of the initial piles
    std::vector< int > const& getInitialBoard() const;

    /// Get the game's current position
    /// @returns the current position
    CrossoutPosition& getCurrentPosition();

    /// Get the game's current position
    /// @returns the current position
    CrossoutPosition const& getCurrentPosition() const;

    /// Gets the max sum
    /// @returns the max sum
    int getMaxSum() const;
    
    /// Is it the human's or computer turns?
    /// @returns true if it is the human's turn, false otherwise
    bool isHumansTurn() const;

    /// Sets if it is the human's or computer's turn
    /// @param turn should be true if it's the human's turn, false otherwise
    void setHumansTurn(bool turn);

    /// Calculates if the given is the final position of the game
    /// @param position the position
    /// @returns true if it is the final position, false otherwise
    bool isFinal(Position const& position) const;

protected:

    /// Generates all of the next possible positions from a given source position
    /// @param position the source position
    /// @returns a vector of all the new positions.
    std::vector< Position > legalPositions(Position const& position) const;

    /// Does nothing. Needed for Evaluate()
    ///// Does nothing. Needed for Evaluate()
    void reverse(Position&) { }

private:

    CrossoutPosition currentPosition;
    int maxNum, maxSum;
    GameMode gameMode;
    std::vector< int > initialBoard;
    bool humansTurn;
};


inline GameMode CrossoutState::getGameMode() const {
    return gameMode;
}

inline std::vector< int > const& CrossoutState::getInitialBoard() const {
    return initialBoard;
}

inline CrossoutPosition& CrossoutState::getCurrentPosition() {
    return currentPosition;
}

inline CrossoutPosition const& CrossoutState::getCurrentPosition() const {
    return currentPosition;
}

inline int CrossoutState::getMaxSum() const {
    return maxSum;
}

inline bool CrossoutState::isHumansTurn() const {
    return humansTurn;
}

inline void CrossoutState::setHumansTurn(bool turn) {
    humansTurn = turn;
}

#endif // CROSSOUT_STATE_H

