// 
// Author:      Jedd Haberstro
// Contributor:	Paul Aman
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#include "Game.h"
#include <iostream>
#include <limits>
#include <cstdlib>

int Game::run(int argc, char* argv[]) {
    if (!initialise(argc - 1, argv + 1)) {
        return 1;
    }

    while (true) {
        if (!update()) {
            break;
        }
    }

    end();
    return 0;
}

void Game::invalidInputRefresh() const {
    if (std::cin.eof()) {
        std::cerr << std::endl << "Unexpected end of input!" << std::endl;
        std::cerr << "Exiting." << std::endl;
        exit(1);
    }
    else {
        std::cout << "Invalid input. Please choose again." << std::endl;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits< std::streamsize >::max(), '\n');
    }
}
