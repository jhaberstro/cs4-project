// 
// Author:      Jedd Haberstro
// Contributor:	Paul Aman
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef GAME_HPP
#define GAME_HPP


/// Enumeration representing what mode the game should be played in.
enum GameMode
{
	GAMEMODE_NORMAL,
	GAMEMODE_INTERACTIVE
};

/// Game class represents a generic game, and provides the framework for all other game types.
class Game
{
public:

    /// run is a generic handler responsible for running a Game
    /// @param argc argument count
    /// @param argv arguments
    int run(int argc, char* argv[]);

    /// initialise is called a program startup.
    /// @param argc the number of arguments passed to the program
    /// @param argv the parameters passed to the program. Excludes the first implicit parameter that stores the name of the program.
    /// @returns false when initialisation fails.
    virtual bool initialise(int argc, char* argv []) = 0;

    /// update is responsible for providing the implementation for the game loop.
    /// @returns false when the program should exit
    virtual bool update() = 0;

    /// end is responsible for reporting any ending messages and destroying any resources used by the game.
    virtual void end() = 0;

protected:

	/// PlayNormal is responsible for the game logic to be performed in update
	/// when the game is being played in Normal mode
	virtual void playNormal() = 0;

	/// PlayInteractive is responsible for the game logic to be performed in
	/// update when the game is being played in Interactive mode (human vs
	/// computer).
	virtual bool playInteractive() = 0;

    /// This prints to the user that the last input was invalid
    /// and then resets std::cin or exits if EOF.. 
    void invalidInputRefresh() const;
};


#endif // GAME_HPP
