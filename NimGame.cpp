// Author:		Paul Aman
// Contributor:	Jedd Haberstro
// Repository:	https://bitbucket.org/jhaberstro/cs4-project/overview
//				git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#include "NimGame.h"
#include "Parsing.h"
#include <iostream>
#include <sstream>
#include <vector>
#include <cstring>
#include <limits>

using namespace std;

NimGame::NimGame(){
}

/// Here the command line parameters are verified and parsed.
bool NimGame::initialise(int argc, char* argv[]) {
	GameMode gameMode;
	vector<int> initialPiles;
	int pile = 0;
	bool failure = false;
	if ((argc > 0) && (strcmp(argv[0], "play") != 0)) {
		gameMode = GAMEMODE_NORMAL;
		for (int i = 0; i < argc; i++) {
			bool parseFailure = !std::parse(argv[i], pile);
			if (parseFailure || pile < 0){
				failure = true;
				break;
			}
			else {
				initialPiles.push_back(pile);
			}
		}
	}
    else if ((argc > 1) && (strcmp(argv[0], "play") == 0)){
        gameMode = GAMEMODE_INTERACTIVE;
		for (int i = 1; i < argc; i++) {
			bool parseFailure = !std::parse(argv[i], pile);
			if (parseFailure || pile <= 0){
				failure = true;
				break;
			}
			else {
				initialPiles.push_back(pile);
			}
		}
	}
    else {
        failure = true;
    }

    if (failure) {
        std::cerr << "usage:\tnim [play] pile1_num_of_pebbles pil2_num_of_pebbles ..." << std::endl;
    }
    else {
        state_.initialise(gameMode, initialPiles);
    }

    return failure == false;
}

bool NimGame::update() {
    switch(state_.getGameMode()) {
        case GAMEMODE_NORMAL: {
            playNormal();
            return false;
        }

        case GAMEMODE_INTERACTIVE: {
            return playInteractive();
        }
    }

    return true;
}

// Reports who won
void NimGame::end() {
    if (state_.getGameMode() == GAMEMODE_INTERACTIVE) {
        std::cout << "No more possible moves left!" << std::endl;
        if (state_.isHumansTurn()) {
            std::cout << "You have won the game!" << std::endl;
        } 
        else {
            std::cout << "The Computer won!" << std::endl;
        }
    }

    std::cout << std::endl;
}

void NimGame::playNormal() {
	NimPosition position(state_.getInitialPiles());
    NimPosition result = state_.evaluate(position);
    int difference = 0;
    int changedPile = -1;
    findChangeInPositions(result, state_.getCurrentPosition(), difference, changedPile);
    if (difference > 0) {
        std::cout << "The computer took away " << difference << " stones from pile " << changedPile + 1 << "." << std::endl;            
    }        
    else {
        std::cout << "No possible moves." << std::endl;
    }
}

bool NimGame::playInteractive() {
    std::cout << "Current piles: ";
	vector<int> currentPiles(state_.getCurrentPosition().getPiles());
	for (int i = 0; i < currentPiles.size(); i++) {
		cout << currentPiles.at(i) << " ";
	}
	std::cout << endl;
    if (state_.isHumansTurn()) {
        bool invalidPileChoice = true;
        int pileChoice;
        int stoneChoice;
		do {
            invalidPileChoice = true;           
            // Print out all of the available moves based on the current stones left
            std::cout << "Choose a non-empty pile(1-" << currentPiles.size() << "):";
            if ((std::cin >> pileChoice)) {
				if (pileChoice > 0 && pileChoice <= currentPiles.size() && currentPiles[pileChoice-1] > 0){
					invalidPileChoice = false;
                    bool invalidStoneChoice = true;
					do {
                        invalidStoneChoice = true;
						std::cout << "Choose a number of stones to take from the pile: ";
						if ((std::cin >> stoneChoice)){
							if (stoneChoice > 0 && stoneChoice <= currentPiles.at(pileChoice-1)){
								invalidStoneChoice = false;
							}
						}
						else {
                            invalidInputRefresh();
			            }
					} while (invalidStoneChoice);
				}
            }
            else {
                invalidInputRefresh();
            }
        } while (invalidPileChoice);
		// needs to be implemented
        state_.getCurrentPosition().getPiles()[pileChoice - 1] -= stoneChoice;
    }
    else {
        // Let the AI choose the next best position
        NimPosition result = state_.evaluate(state_.getCurrentPosition());
        int difference = 0;
        int changedPile = -1;
        findChangeInPositions(result, state_.getCurrentPosition(), difference, changedPile);
        if (difference > 0) {
            std::cout << "The computer took away " << difference << " stones from pile " << changedPile + 1 << "." << std::endl;            
        }
        
        state_.getCurrentPosition() = result;
	}

    // End the game if there are no pennies/moves left
    if (isFinalPosition(state_.getCurrentPosition())) {
        return false;
    } 
    else {
        state_.setHumansTurn(!state_.isHumansTurn());
    }

    std::cout << std::endl;
    return true;
}

bool NimGame::isFinalPosition(NimPosition const& position) {
    int sum = 0;
    std::vector< int > const& piles = position.getPiles();
    for (int i = 0; i < piles.size(); ++i) {
        sum += piles[i];
    }

    return sum == 0;
}

void NimGame::findChangeInPositions(
    NimPosition const& newPosition,
    NimPosition const& originalPosition,
    int& difference,
    int& changedPile
) {
    std::vector< int > const& newPiles = newPosition.getPiles();
    std::vector< int > const& originalPiles = originalPosition.getPiles();
    for (int i = 0; i < newPiles.size(); ++i) {
        if (newPiles[i] != originalPiles[i]) {
            changedPile = i;
            difference = originalPiles[i] - newPiles[i];
            break;
        }
    }
}
