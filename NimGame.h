// Author:		Paul Aman	
// Contributor:	Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef NIMGAME_HPP
#define NIMGAME_HPP

#include "Game.h"
#include "StateController.h"
#include "NimState.h"

/// Game implementation for takeaway
/// @author: Paul Aman
class NimGame : public Game
{
public:
	
	/// NimGame constructor
	NimGame();

	/// See Game::initialise
	virtual bool initialise(int argc, char* argv[]);

    /// See Game::update
    virtual bool update();

	/// See Game::end
    virtual void end();

private:
	
	/// PlayNormal is responsible for the game logic to be performed in update
	/// when the game is being played in Normal mode
	virtual void playNormal();

	/// PlayInteractive is responsible for the game logic to be performed in
	/// update when the game is being played in Interactive mode (human vs
	/// computer).
	virtual bool playInteractive();

    /// Check if a given position is the final position
    /// @returns true if the given position is the final position, false otherwise
    bool isFinalPosition(NimPosition const& position);
    
    /// Find the pile that has changed between the two positions and their delta.
    /// @param newPosition the new position with the changed pile
    /// @param originalPosition the original position with source piles
    /// @param difference stores the result of the delta
    /// @param stores the index to which pile changed
    void findChangeInPositions(
        NimPosition const& newPosition,
        NimPosition const& originalPosition,
        int& difference,
        int& changedPile
    ); 


private:

	/// All of the state of the game
	StateController< NimState > state_;
};

#endif // NIMGAME_HPP
