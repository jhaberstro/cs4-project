// 
// Author:      Jedd Haberstro
// Contributor: Paul Aman
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef NIM_POSITION_H
#define NIM_POSITION_H

#include <vector>
#include <string>
#include <cstring>
#include <sstream>

class NimState;

/// NimPosition represents a single position in the game of Nim.
/// @author Jedd Haberstro
class NimPosition
{
    friend class NimState;

public:
    /// Constructs a new position
    /// @param piles a vector of int representing the value of each pile of pebbles for this position
    NimPosition(std::vector< int > const& piles);
    
    NimPosition(NimPosition const& other);

    /// Gets this position's pile configuration
    /// @returns vector of int representing the piles
    std::vector< int >& getPiles();

    /// Gets this position's pile configuration
    /// @returns vector of int representing the piles
    std::vector< int > const& getPiles() const;

    /// negates the value of use in Evaluate
    void negate();

    /// Reports whether this is a winning move.
    /// @returns true if the move is a winning move, else false
    bool isWin() const; 

    /// Score is simply a win or lose. Set the score to win.
    void incrementScore(); 

    /// Score is simply a win or lose. Set the score to lose..
    void decrementScore(); 

    /// Sets the score to the appropriate value for a final position
    void setScoreForFinalPosition();
    
    /// Compares two positions
    /// @param other position to compare against
    bool isWorthMore(NimPosition const& other) const;

    /// Compares two positions
    /// @param other position to compare against
    bool operator< (NimPosition const& other) const;

protected:
    
    /// Constructor for  empty initialisation
    NimPosition(); 

private:

    std::vector< int > piles;
    bool score;
};

inline NimPosition::NimPosition()
: score(false) {
}

inline NimPosition::NimPosition(std::vector< int > const& piles)
: piles(piles),
  score(false){
}

inline NimPosition::NimPosition(NimPosition const& other)
: piles(other.piles),
  score(other.score) {
}


inline std::vector< int >& NimPosition::getPiles() {
    return piles;
}

inline std::vector< int > const& NimPosition::getPiles() const {
    return piles;
}

inline void NimPosition::negate() {
}

inline bool NimPosition::isWin() const {
    return score;
}

inline void NimPosition::incrementScore() {
    score = true;
}

inline void NimPosition::decrementScore() {
    score = false;
}

inline void NimPosition::setScoreForFinalPosition() {
    score = false;
}

inline bool NimPosition::isWorthMore(NimPosition const& other) const {
    return !score;
}

inline bool NimPosition::operator< (NimPosition const& other) const {
    return piles < other.piles; 
}

#endif // NIM_POSITION_H

