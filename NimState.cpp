// 
// Author:      Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#include "NimState.h"
#include <iostream>

void NimState::initialise(GameMode mode, std::vector< int > const& initialPiles) {
    this->initialPiles = initialPiles;
    this->gameMode_ = mode;
    humansTurn = true;
    currentPosition = Position(initialPiles);
}

bool NimState::isFinal(NimPosition const& position) const {
    int sum = 0;
    std::vector< int > const& piles = position.getPiles();
    for (int i = 0; i < piles.size(); ++i) {
        sum += piles[i];
    }

    return (sum == 0);
}

std::vector< NimPosition > NimState::legalPositions(NimPosition const& position) const {
    std::vector< int > const& positionsPiles = position.getPiles();
    std::vector< Position > result;
    for (int i = 0; i < positionsPiles.size(); ++i) {
        int pebbles = positionsPiles[i];
        for (int j = 1; j <= pebbles; ++j) {
            std::vector< int > pileCopy = positionsPiles;
            pileCopy[i] -= j;
            result.push_back(Position(pileCopy));
        }
    }

    return result;
}
