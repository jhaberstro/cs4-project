// 
// Author:      Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef NIM_STATE_H
#define NIM_STATE_H

#include "NimPosition.h"
#include "Game.h"

/// NimState represents all of the state for the game nim
/// @author: Jedd Haberstro
class NimState
{
protected:

    typedef NimPosition Position;

public:

    /// Initialises all of the state
    /// @param mode which mode the game will be played in
    /// @param initialPiles the piles of pebbles the game is launched with.
    void initialise(GameMode mode, std::vector< int > const& initialPiles);

    /// Gets the game mode
    /// @returns the mode
    GameMode getGameMode() const;

    /// Gets the initial piles the game is launched with
    /// @returns vector of the initial piles
    std::vector< int > const& getInitialPiles() const;

    /// Get the game's current position
    /// @returns the current position
    NimPosition& getCurrentPosition();

    /// Is it the human's or computer turns?
    /// @returns true if it is the human's turn, false otherwise
    bool isHumansTurn() const;

    /// Sets if it is the human's or computer's turn
    /// @param turn should be true if it's the human's turn, false otherwise
    void setHumansTurn(bool turn);

protected:

    /// Calculates if the given is the final position of the game
    /// @param position the position
    /// @returns true if it is the final position, false otherwise
    bool isFinal(Position const& position) const;

    /// Generates all of the next possible positions from a given source position
    /// @param position the source position
    /// @returns a vector of all the new positions.
    std::vector< Position > legalPositions(Position const& position) const;

    /// Does nothing. Needed for Evaluate()
    ///// Does nothing. Needed for Evaluate()
    void reverse(Position&) { }

private:

    Position currentPosition;
    GameMode gameMode_;
    std::vector< int > initialPiles;
    bool humansTurn;
};

inline GameMode NimState::getGameMode() const {
    return gameMode_;
}

inline std::vector< int > const& NimState::getInitialPiles() const {
    return initialPiles;
}

inline NimPosition& NimState::getCurrentPosition() {
    return currentPosition;
}

inline bool NimState::isHumansTurn() const {
    return humansTurn;
}

inline void NimState::setHumansTurn(bool turn) {
    humansTurn = turn;
}

#endif // NIM_STATE_H
