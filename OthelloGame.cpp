// 
// Author:      Jedd Haberstro
// Contributor: Paul Aman
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#include "OthelloGame.h"
#include "OthelloPosition.h"
#include <fstream>
#include <cstring>

bool OthelloGame::initialise(int argc, char* argv[]) {
    GameMode gameMode;
    bool failure = false;
    char* boardParameter = 0;
    if (argc == 1) {
        gameMode = GAMEMODE_NORMAL;
        boardParameter = argv[0];
    }
    else if (argc == 2 && strcmp(argv[0], "play") == 0) {
        gameMode = GAMEMODE_INTERACTIVE;
        boardParameter = argv[1];
    }
    else {
        failure = true;
    }

    std::istream* input = 0;
    if (!failure) {
        input = getInputStream(boardParameter);
        if (input == 0) {
            failure = true;
        }
    }

    if (failure) {
        std::cerr << "usage:\tothello [play] { - | file-name }" << std::endl;
    }
    else {
        int rows = -1, columns = -1;
        std::vector< BoardValue > board = parseBoard(*input, &rows, &columns);
        state_.initialise(gameMode, board, rows, columns);
        if (input != &std::cin) {
            delete input;
        }
    }

    return failure == false;
}

bool OthelloGame::update() {
    switch(state_.getGameMode()) {
        case GAMEMODE_NORMAL: {
            playNormal();
            return false;
        }

        case GAMEMODE_INTERACTIVE: {
            return playInteractive();
        }
    }

    return true;
} 

void OthelloGame::end() {
    if (state_.getGameMode() == GAMEMODE_NORMAL) {
        return;
    }

    std::cout << std::endl;
    int score = 0;
    int rows = state_.getCurrentPosition().getRows();
    int columns = state_.getCurrentPosition().getColumns();
    for (int r = 0; r < rows; ++r) {
        for (int c = 0; c < columns; ++c) {
            if (state_.getCurrentPosition().getTile(r, c) == X) {
                score += 1;
            }
        }
    }
    
    int half = (rows * columns) / 2;
    if (score > half) {
        std::cout << "You have won the game!" << std::endl;
    }
    else if (score < half) {
        std::cout << "The computer won the game." << std::endl;
    }
    else {
        std::cout << "It's a draw!";
    }

}

std::istream* OthelloGame::getInputStream(char* parameter) {
    if (strcmp(parameter, "-") == 0) {
        return &std::cin;
    }
    else {
        std::ifstream* result = new std::ifstream(parameter);
        if (result->good()) {
            return result;
        }
        else {
            delete result;
            return 0;
        }
    }
}

std::vector< BoardValue > OthelloGame::parseBoard(std::istream& stream, int* outRows, int* outColumns) {
    int columns = -1, rows = -1;
    stream >> columns;
    stream >> rows;
    std::vector< BoardValue > board(rows * columns);
    for (int r = 0; r < rows; ++r) {
        for (int c = 0; c < columns; ++c) {
            BoardValue* tile = &board[(r * columns) + c];
            std::string val;
            stream >> val;
            if (val == ".") {
                *tile = EMPTY;
            }
            else if (val == "X") {
                *tile = X;
            }
            else {
                *tile = O;
            }
        }
    }

    *outRows = rows;
    *outColumns = columns;
    return board;
}

void OthelloGame::playNormal() {
    std::cout << "Starting board:" << std::endl;
    state_.getCurrentPosition().print();
    std::cout << std::endl << "New board after computer: " << std::endl;

    OthelloPosition reversed(state_.getCurrentPosition());
    state_.reverse(reversed);
    OthelloPosition result = state_.evaluate(reversed);
    state_.reverse(result);
    result.print();
}

bool OthelloGame::playInteractive() {
    OthelloPosition& currentPosition = state_.getCurrentPosition();
    std::cout << "-----------------------------" << std::endl;
    std::cout << "Current board:" << std::endl;
    currentPosition.print();
    std::cout << std::endl;

    bool finished = true;
    for (int r = 0; r < currentPosition.getRows(); ++r) {
        for (int c = 0; c < currentPosition.getColumns(); ++c) {
            if (currentPosition.getTile(r, c) == EMPTY) {
                finished = false;
                break;
            }
        }

         if (finished == false) {
             break;
         }
    }

    if (finished) {
        return false;
    }  

    if (state_.isHumansTurn()) {
        bool invalidChoice = true;
        int rowChoice = -1, columnChoice = -1;
        int rows = currentPosition.getRows();
        int columns = currentPosition.getColumns();
        do {
            std::cout << "Please enter a row and column." << std::endl;
            std::cout << "   Row: ";
            if (std::cin >> rowChoice) {
                std::cout << "   Column: ";
                if (std::cin >> columnChoice) {
                    std::cout << std::endl;
                    invalidChoice = (rowChoice < 0) || (columnChoice < 0) || (rowChoice >= rows) || (columnChoice >= columns);
                    if (invalidChoice) {
                        std::cout << "Invalid row or column. ";
                    }
                }
                else {
                    invalidInputRefresh();
                }
            }
            else {
                invalidInputRefresh();
            }
        } while (invalidChoice);

        currentPosition.setTile(rowChoice, columnChoice, X);
        std::cout << "The board after your move is now:" << std::endl;
        currentPosition.print();
    }
    else {
        OthelloPosition reversed(currentPosition);
        state_.reverse(reversed);
        OthelloPosition result = state_.evaluate(reversed);
        state_.reverse(result);

        std::cout << "The board after the computer's move is now:" << std::endl;
        result.print();
        currentPosition = OthelloPosition(result);
    }

    state_.setHumansTurn(!state_.isHumansTurn());

    std::cout << std::endl;
    return true;
}
