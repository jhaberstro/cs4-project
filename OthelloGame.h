// 
// Author:      Jedd Haberstro
// Contributor: Paul Aman
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef OTHELLOGAME_HPP
#define OTHELLOGAME_HPP

#include "Game.h"
#include "StateController.h"
#include "OthelloState.h"
#include <iostream>

// @author Jedd Haberstro
class OthelloGame : public Game
{
public:

	/// See Game::initialise
    bool initialise(int argc, char* argv[]);

    /// See Game::update
    bool update();

	/// See Game::end
    void end();

protected:

	/// PlayNormal is responsible for the game logic to be performed in update
	/// when the game is being played in Normal mode
    virtual void playNormal();

	/// PlayInteractive is responsible for the game logic to be performed in
	/// update when the game is being played in Interactive mode (human vs
	/// computer).
    virtual bool playInteractive();

private:

    /// Returns the approriate stream which to read parse the board from.
    /// @param parameter parameter that determines whether the board will come
    ///         standard input or a file.
    /// @returns the input stream
    std::istream* getInputStream(char* parameter);
    
    /// Parses the board file
    /// @param stream the input stream
    /// @param outRows stores the num rows on finish
    /// @param outColumns stores the num columns on finish
    /// @param the filled out board
    std::vector< BoardValue > parseBoard(std::istream& stream, int* outRows, int* outColumns); 

private:

    StateController< OthelloState > state_;

};

#endif // OTHELLOGAME_HPP
