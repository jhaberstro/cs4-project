
// Author:      Jedd Haberstro
// Contributor: Paul Aman
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#include "OthelloPosition.h"

void OthelloPosition::setTile(int row, int column, BoardValue value) {
    assert(row >= 0 && row < rows);
    assert(column >= 0 && column < columns);
    board[(row * columns) + column] = value;

    BoardValue oppValue = (value == X ? O : X);
    std::stack< std::pair< int, int > > tilesChange;
    std::pair< int, int > directions[8] = {
        std::make_pair(1, 0),   // down direction
        std::make_pair(-1, 0),  // up direction
        std::make_pair(0, 1),   // right direction
        std::make_pair(0, -1),  // left direction
        std::make_pair(1, 1),   // down-right direction
        std::make_pair(1, -1),  // down-left direction
        std::make_pair(-1, 1),  // up-right direction
        std::make_pair(-1, -1), // up-left direction
    };

    for (int i = 0; i < 8; ++i) {
        int r = row + directions[i].first, c = column + directions[i].second;
        int count = 0;
        while (
            r < rows &&
            r >= 0 &&
            c < columns &&
            c >= 0 &&
            board[(r * columns) + c] == oppValue
        ) {
            tilesChange.push(std::make_pair(r, c));
            r += directions[i].first;
            c += directions[i].second;
            count += 1;
        }

        if (
            r == rows ||
            r == -1 ||
            c == columns ||
            c == -1 ||
            board[(r * columns) + c] == EMPTY
        ) {
            while (count--) tilesChange.pop();
        }
    }

    while (tilesChange.size()) {
        std::pair< int, int > rc = tilesChange.top();
        tilesChange.pop();
        board[(rc.first * columns) + rc.second] = value;
    }
}

void OthelloPosition::print() const {
    std::cout << "   ";
    for (int i = 0; i < columns; ++i) std::cout << i << " ";
    std::cout << std::endl;
    char charLookup[] = { '.', 'X', 'O' };
    for (int r = 0; r < rows; ++r) {
        std::cout << r << "  ";
        for (int c = 0; c < columns; ++c) {
            std::cout << charLookup[board[(r * columns) + c]] << " ";
        }
        std::cout << std::endl;
    }
}

