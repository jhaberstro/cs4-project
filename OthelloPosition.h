// 
// Author:      Jedd Haberstro
// Contributor: Paul Aman
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef OTHELLO_POSITION_H
#define OTHELLO_POSITION_H

#include <vector>
#include <cassert>
#include <stack>
#include <iostream>

enum BoardValue
{
    EMPTY,
    X,
    O
};

class OthelloState;

/// @author Jedd Haberstro
class OthelloPosition
{
    
    friend class OthelloState;

public:

    /// Default constructor
    OthelloPosition();

    /// Constructor
    /// @para board the board
    /// @param rows number of rows
    /// @param columns number of columns/ 
    OthelloPosition(std::vector< BoardValue > const& board, int rows, int columns);

    /// Copy constructor
    /// @param other instance
    OthelloPosition(OthelloPosition const& other);

    /// Changes the value of a tile in the board, and then captures any pieces
    /// @param row the row
    /// @param column the column
    /// @param the value to set the tile to
    void setTile(int row, int column, BoardValue value);

    /// Gets the number of rows in the board
    /// @returns number of rows
    int getRows() const;

    /// Get the number of columns
    /// @returns the number of columns
    int getColumns() const;

    /// Gets the value of a tile
    /// @param r the row
    /// @param c the column
    /// @returns the value of the tile
    BoardValue getTile(int r, int c) const;

    /// negates the value of use in Evaluate
    void negate();

    /// Reports whether this is a winning move.
    /// @returns true if the move is a winning move, else false
    bool isWin() const; 

    /// Score is simply a win or lose. Set the score to win.
    void incrementScore(); 

    /// Score is simply a win or lose. Set the score to lose..
    void decrementScore(); 

    /// Sets the score to the appropriate value for a final position
    void setScoreForFinalPosition();
    
    /// Compares two positions
    /// @param other position to compare against
    bool isWorthMore(OthelloPosition& other);

    /// Compares two positions
    /// @param other position to compare against
    bool operator< (OthelloPosition const& other) const;

    /// Prints the board contents
    void print() const;

    /// Calculate's the score
    void calculateScore(BoardValue value = X); 

private:

    std::vector< BoardValue > board;
    int rows, columns;
    int score;
    bool isWinner;
};

inline OthelloPosition::OthelloPosition()
: rows(-1),
  columns(-1),
  score(0),
  isWinner(false) {
}

inline OthelloPosition::OthelloPosition(std::vector< BoardValue > const& board, int rows, int columns)
: board(board),
  rows(rows),
  columns(columns),
  score(0),
  isWinner(false) {
    calculateScore();
}

inline OthelloPosition::OthelloPosition(OthelloPosition const& other)
: board(other.board),
  rows(other.rows),
  columns(other.columns),
  score(other.score),
  isWinner(other.isWinner) {
}

inline int OthelloPosition::getRows() const {
    return rows;
}

inline int OthelloPosition::getColumns() const {
    return columns;
}

inline BoardValue OthelloPosition::getTile(int r, int c) const {
    return board[(r * columns) + c];
}

inline void OthelloPosition::calculateScore(BoardValue value) {
    score = 0;
    for (int i = 0; i < rows * columns; ++i) {
        if (board[i] == value) {
            score += 1;
        }
    }
}

inline void OthelloPosition::negate() {
    isWinner = !isWinner;
    calculateScore(O);
}

inline bool OthelloPosition::isWin() const {
    return isWinner;
}

inline void OthelloPosition::incrementScore() {
    isWinner = true;
}

inline void OthelloPosition::decrementScore() {
    isWinner = false;
}

inline void OthelloPosition::setScoreForFinalPosition() {
    calculateScore();
    isWinner = (score > ((rows * columns) / 2));
}

inline bool OthelloPosition::isWorthMore(OthelloPosition& other) {
    if (isWinner == other.isWinner) {
        calculateScore();
        other.calculateScore();
        return score > other.score;
    }

    return isWinner;
}

inline bool OthelloPosition::operator< (OthelloPosition const& other) const {
    return board < other.board;
}

#endif // OTHELLO_POSITION_H
