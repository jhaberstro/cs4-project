// 
// Author:      Jedd Haberstro
// Contrubitor: Paul Aman
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#include "OthelloState.h"

void OthelloState::initialise(GameMode mode, std::vector< BoardValue > const& initialBoard, int rows, int columns) {
    currentPosition_ = Position(initialBoard, rows, columns);
    gameMode_ = mode;
    initialBoard_ = Position(initialBoard, rows, columns);
    humansTurn_ = true;
}

bool OthelloState::isFinal(Position const& position) const {
    for (int c = 0; c < position.getColumns(); ++c) {
        for (int r = 0; r < position.getRows(); ++r) {
            if (position.getTile(r, c) == EMPTY) {
                return false;
            }
        }
    }

    return true;
}

std::vector< OthelloPosition > OthelloState::legalPositions(OthelloPosition const& position) const {
    std::vector< OthelloPosition > positions;
    BoardValue value = X; //(humansTurn_ ? X : O);

    int count = 0;
    for (int c = 0; c < position.getColumns(); ++c) {
        for (int r = 0; r < position.getRows(); ++r) {
            if (position.getTile(r, c) == EMPTY) {
                OthelloPosition newPos(position);
                newPos.setTile(r, c, value);
                positions.push_back(newPos);
            }
        }
    }

    return positions;
}

void OthelloState::reverse(OthelloPosition& position) {
    // reverse board tiles and recalculate score
    int newScore = 0;
    for (int i = 0; i < position.board.size(); ++i) {
        if (position.board[i] == X) {
            position.board[i] = O;
        }
        else if (position.board[i] == O) {
            position.board[i] = X;
            ++newScore;
        }
    }

    position.score = newScore;
}




