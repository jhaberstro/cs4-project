// 
// Author:      Jedd Haberstro
// Contrubitor: Paul Aman
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef OTHELLO_STATE_H
#define OTHELLO_STATE_H

#include "OthelloPosition.h"
#include "Game.h"

/// OthelloState represents all of the state for the game othello
/// @author: Jedd Haberstro
class OthelloGame;
class OthelloState
{
    friend class OthelloGame;

public:

    typedef OthelloPosition Position;

public:

    /// Initialises all of the state
    /// @param mode which mode the game will be played in
    /// @param initialBoard the piles of pebbles the game is launched with.
    void initialise(GameMode mode, std::vector< BoardValue > const& initialBoard, int rows, int columns);

    /// Gets the game mode
    /// @returns the mode
    GameMode getGameMode() const;

    /// Gets the n initial board
    /// @returns vector of the initial board
    OthelloPosition const& getInitialBoard() const;

    /// Get the game's current position
    /// @returns the current position
    OthelloPosition& getCurrentPosition();

    /// Is it the human's or computer turns?
    /// @returns true if it is the human's turn, false otherwise
    bool isHumansTurn() const;

    /// Sets if it is the human's or computer's turn
    /// @param turn should be true if it's the human's turn, false otherwise
    void setHumansTurn(bool turn);

protected:

    /// Calculates if the given is the final position of the game
    /// @param position the position
    /// @returns true if it is the final position, false otherwise
    bool isFinal(Position const& position) const;

    /// Generates all of the next possible positions from a given source position
    /// @param position the source position
    /// @returns a vector of all the new positions.
    std::vector< Position > legalPositions(Position const& position) const;

    /// Reverse the passed position 
    /// @param position the position to reverse
    void reverse(Position& position);

private:

    Position currentPosition_;
    GameMode gameMode_;
    Position initialBoard_;
    bool humansTurn_;
};

inline GameMode OthelloState::getGameMode() const {
    return gameMode_;
}

inline OthelloPosition const& OthelloState::getInitialBoard() const {
    return initialBoard_;
}

inline OthelloPosition& OthelloState::getCurrentPosition() {
    return currentPosition_;
}

inline bool OthelloState::isHumansTurn() const {
    return humansTurn_;
}

inline void OthelloState::setHumansTurn(bool turn) {
    humansTurn_ = turn;
}

#endif // OTHELLO_STATE_H

