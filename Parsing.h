// 
// Author:      Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef PARSING_H
#define PARSING_H

#include <sstream>

namespace std
{
    /// Parses a value from a string
    /// @return true on success, false on failure
    template< typename TParseResult >
    bool parse(std::string const& str, TParseResult& result) {
        std::istringstream stream(str);
        return (stream >> result);
    }
}

#endif // PARSING_H
