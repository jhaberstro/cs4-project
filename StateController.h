// 
// Author:      Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//


#ifndef STATECONTROLLER_H
#define STATECONTROLLER_H

#include <set>
#include <vector>

/// A generic state class that implements the Evaluate functionality
/// @author Jedd Haberstro
template< typename TStateParentImplementor >
class StateController : public TStateParentImplementor
{
    typedef typename TStateParentImplementor::Position Position;
    
public: 

    /// Evaluate will analyze the passed in position and find the next position that has the highest likely hooding of helping the caller win the game.
    ///// @param position the current position of the caller in the game
    ///// @returns the best possible next position
    Position evaluate(Position& position);

private:

    std::set< Position > positionCache_;
};


template< typename T >
typename T::Position StateController< T >::evaluate(typename T::Position& position) {
    if (isFinal(position)) {
        position.setScoreForFinalPosition();
        return position;
    }
    else {
        std::vector< Position > legalPositionsVec = legalPositions(position);
        
        // Loop through all of the next legal positions and calculate the score of 'position'
        // and also find the best next position
        typename std::vector< Position >::iterator itr;
        bool hasWin = false;
        bool hasLose = false;
        Position* best = &(*legalPositionsVec.begin());
        for (itr = legalPositionsVec.begin(); itr != legalPositionsVec.end(); ++itr) {
            Position* newPosition = &(*itr);
            typename std::set< Position >::iterator cached = positionCache_.find(*newPosition);
            if (cached != positionCache_.end()) {
                *newPosition = *cached; 
            }
            else {
                // Evaluate as a side effect will calculate the score for newPosition.
                // Since we are comparing each possible newPosition, we don't care
                // about the return value
                //reverse(*newPosition);
                evaluate(*newPosition);
                positionCache_.insert(*newPosition);
            }

            //newPosition->negate();
            if (newPosition->isWin()) {
                hasWin = true;
            }
            else {
                hasLose = true;
            }

            if (newPosition->isWorthMore(*best)) {
                best = newPosition;
            } 
        }

        if (hasWin && !hasLose) {
            position.decrementScore();
        }
        else {
            position.incrementScore();
        }

        return *best;
    }
}

#endif // STATECONTROLLER_H
