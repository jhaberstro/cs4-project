// 
// Author:      Jedd Haberstro
// Contributor:	Paul Aman
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#include "TakeAwayGame.h"
#include "Parsing.h"
#include <iostream>
#include <sstream>
#include <vector>
#include <cstring>
#include <limits>

TakeAwayGame::TakeAwayGame() {
}

/// Here the command line parameters are verified and parsed.
bool TakeAwayGame::initialise(int argc, char* argv[]) {
    GameMode gameMode;
    int initialPennies;
    bool failure = false;
    if (argc == 1) {
        gameMode = GAMEMODE_NORMAL;
        bool parseFailure = !std::parse(argv[0], initialPennies);
        failure = (parseFailure) || (initialPennies < 0);
    }
    else if (argc == 2) {
        gameMode = GAMEMODE_INTERACTIVE; 
        int param1 = strcmp(argv[0], "play");
        bool parseFailure = !std::parse(argv[1], initialPennies);
        failure = (parseFailure) || (param1 != 0) || (initialPennies < 0);
    }
    else {
        failure = true;
    }

    if (failure) {
        std::cerr << "usage:\ttakeaway [play] num_pennies" << std::endl;
    }
    else {
        state_.initialise(gameMode, initialPennies);
    }

    return failure == false;
}

bool TakeAwayGame::update() {
    switch(state_.getGameMode()) {
        case GAMEMODE_NORMAL: {
            playNormal();
            return false;
        }

        case GAMEMODE_INTERACTIVE: {
            return playInteractive();
        }
    }

    return true;
}

// Reports who won
void TakeAwayGame::end() {
    if (state_.getGameMode() == GAMEMODE_INTERACTIVE) {
        std::cout << "No more possible moves left!" << std::endl;
        if (state_.isHumansTurn()) {
            std::cout << "The Computer won!" << std::endl;
        } 
        else {
            std::cout << "You have won the game!" << std::endl;
        }
    }

    std::cout << std::endl;
}

void TakeAwayGame::playNormal() {
    TakeAwayPosition position(state_.getInitialPennies());
    TakeAwayPosition result = state_.evaluate(position);
    if (result.getPosition() == state_.getInitialPennies()) {
        std::cout << "No possible moves." << std::endl;
    }
    else {
        std::cout << "Next position: " << result.getPosition() << std::endl;
    }
}

bool TakeAwayGame::playInteractive() {
    std::cout << "Current pennies pile: " << state_.getCurrentPennies() << std::endl;
    if (state_.isHumansTurn()) {
        int currentPennies = state_.getCurrentPennies();
        enum { INVALID_MOVE = 0 }; 
        int move = INVALID_MOVE;
        while (move == INVALID_MOVE) {
            int moves[3] = { INVALID_MOVE };

            // Print out all of the available moves based on the current pennies left
            std::cout << "Available moves:" << std::endl;
            if ((currentPennies % 2 == 0) && (currentPennies % 3 == 0)) {
                moves[0] = currentPennies / 2;
                moves[1] = currentPennies / 3, moves[2] = 1;
                std::cout << "\t1. Take half" << std::endl;
                std::cout << "\t2. Take one third" << std::endl;
                std::cout << "\t3. Take one" << std::endl;
            }
            else if (currentPennies % 2 == 0) {
                moves[0] = currentPennies / 2, moves[1] = 1;
                std::cout << "\t1. Take half" << std::endl;
                std::cout << "\t2. Take one" << std::endl;
            }
            else if (currentPennies % 3 == 0) {
                moves[0] = currentPennies / 3, moves[1] = 1;
                std::cout << "\t1. Take one third" << std::endl;
                std::cout << "\t2. Take one" << std::endl;
            }
            else {
                moves[0] = 1;
                std::cout << "\t1. Take one" << std::endl;
            }

            // Collect the input
            int choice = 0;
            std::cout << "Please enter your move: ";
            bool parsed = (std::cin >> choice);
            if (parsed && choice > 0) {
                move = moves[choice - 1];
            }
            else {
                invalidInputRefresh();
            }
        }

        state_.setCurrentPennies(currentPennies - move);
    }   
    else {
        // Let the AI choose the next best position
        TakeAwayPosition position(state_.getCurrentPennies());
        TakeAwayPosition result = state_.evaluate(position);
        std::cout << "Computer took away " 
                  << state_.getCurrentPennies() - result.getPosition()
                  << " pennies." << std::endl;
        state_.setCurrentPennies(result.getPosition());
    }

    // End the game if there are no pennies/moves left
    if (state_.getCurrentPennies() == 0) {
        return false;
    } 
    else {
        state_.setHumansTurn(!state_.isHumansTurn());
    }

    std::cout << std::endl;
    return true;
}

