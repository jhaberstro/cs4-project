// 
// Author:      Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef TAKEAWAYGAME_HPP
#define TAKEAWAYGAME_HPP

#include "Game.h"
#include "TakeAwayState.h"
#include "StateController.h"


/// Game implementation for takeaway
/// @author: Jedd Haberstro
class TakeAwayGame : public Game
{
public:

    /// TakeAwayGame constructor
    TakeAwayGame();

    /// See Game::initialise
    virtual bool initialise(int argc, char* argv[]);

    /// See Game::update
    virtual bool update();

    /// See Game::end
    virtual void end();
    
protected:
    
	/// See Game::playNormal
	virtual void playNormal();
    
	/// See Game::playInteractive
	virtual bool playInteractive();    

private:

    /// All of the state of the game
    StateController< TakeAwayState > state_;
};

#endif // TAKEAWAYGAME_HPP

