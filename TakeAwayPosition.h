// 
// Author:      Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef TAKEAWAYPOSITION_H
#define TAKEAWAYPOSITION_H

/// TakeAwayPosition representation a move and position
/// @author: Jedd Haberstro
class TakeAwayPosition
{
public:

    /// Constructor
    /// @param p the value for the position (number of pennies)
    TakeAwayPosition(int p);

    /// negates the value of use in Evaluate
    void negate();

    /// Gets the value of the position
    /// @returns the position
    int getPosition() const; 

    /// Reports whether this is a winning move.
    /// @returns true if the move is a winning move, else false
    bool isWin() const; 

    /// Score is simply a win or lose. Set the score to win.
    void incrementScore(); 

    /// Score is simply a win or lose. Set the score to lose..
    void decrementScore(); 

    /// Sets the score to the appropriate value for a final position
    void setScoreForFinalPosition();
    
    /// Compares two positions
    /// @param other position to compare against
    bool isWorthMore(TakeAwayPosition const& other) const;

    /// Compares two positions
    /// @param other position to compare against
    bool operator< (TakeAwayPosition const& other) const;
    
private:

    int position;
    bool score;
};


inline TakeAwayPosition::TakeAwayPosition(int p)
: position(p),
  score(0) {
}

/// negates the value of use in Evaluate
inline void TakeAwayPosition::negate() {
    //score = !score;
}

/// Gets the value of the position
/// @returns the position
inline int TakeAwayPosition::getPosition() const {
    return position;
}

/// Reports whether this is a winning move.
/// @returns true if the move is a winning move, else false
inline bool TakeAwayPosition::isWin() const {
    return score;
}

/// Score is simply a win or lose. Set the score to win.
inline void TakeAwayPosition::incrementScore() {
    score = true;
} 

/// Score is simply a win or lose. Set the score to lose..
inline void TakeAwayPosition::decrementScore() {
    score = false;
}

inline void TakeAwayPosition::setScoreForFinalPosition() {
    score = true;
}

/// Compares two positions
/// @param other position to compare against
inline bool TakeAwayPosition::isWorthMore(TakeAwayPosition const& other) const {
    return !score;
}

inline bool TakeAwayPosition::operator< (TakeAwayPosition const& other) const {
    return position < other.position;
}

#endif // TAKEAWAYPOSITION_H

