// 
// Author:      Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#include "TakeAwayState.h"

bool TakeAwayState::isFinal(Position const& position) const {
    return position.getPosition() == 0;
}

std::vector< TakeAwayPosition > TakeAwayState::legalPositions(TakeAwayPosition const& position) const {
    std::vector< TakeAwayPosition > result;
    result.push_back(TakeAwayPosition(position.getPosition() - 1));
    if (position.getPosition() % 2 == 0) {
        int value = position.getPosition() - (position.getPosition() / 2);
        if (value != 1) {
            result.push_back(value);
        }
    }
    if (position.getPosition() % 3 == 0) {
        int value = position.getPosition() - (position.getPosition() / 3);
        if (value != 1) {
            result.push_back(value);
        }
    }

    return result;
}
