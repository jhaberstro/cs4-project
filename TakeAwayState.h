// 
// Author:      Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#ifndef TAKEAWAYSTATE_H
#define TAKEAWAYSTATE_H

#include "TakeAwayPosition.h"
#include "Game.h"
#include <vector>

/// TakeAwayState represents all of the state for the game takeaway
/// @author: Jedd Haberstro
class TakeAwayState
{
protected:

    typedef TakeAwayPosition Position;

public:

    /// Initialises all of the state
    /// @param mode which mode the game will be played in
    /// @param pennies the initial penny count
    void initialise(GameMode mode, int pennies);

    /// Gets the game mode
    /// @returns the mode
    GameMode getGameMode() const;

    /// Gets the initial penny count of the game
    /// @returns the penny count
    int getInitialPennies() const;

    /// Gets how many pennies are currently in the pile
    /// @returns the current penny count
    int getCurrentPennies() const;

    /// Who's turn is it?
    /// @returns true if it is the human's turn, false if it is the computer's turn
    bool isHumansTurn() const;
    
    /// Sets how many pennies there are in the pile
    /// @param pennies the new pennies value
    void setCurrentPennies(int pennies);

    /// Sets whose turn it is
    /// @param humansTurn new human's turn boolean state
    void setHumansTurn(bool humansTurn);

protected:

    /// Calculates if the given is the final position of the game
    /// @param position the position
    /// @returns true if it is the final position, false otherwise
    bool isFinal(Position const& position) const;

    /// Generates all of the next possible positions from a given source position
    /// @param position the source position
    /// @returns a vector of all the new positions.
    std::vector< Position > legalPositions(Position const& position) const;

    /// Does nothing. Needed for Evaluate()
    ///// Does nothing. Needed for Evaluate()
    void reverse(Position&) { }

private:

    /// The game mode to be played
    GameMode gameMode_;

    /// The amount of pennies the game is started with
    int initialPennies_;

    /// The amount of pennies currently left
    int currentPennies_;

    /// Boolean keeping track of who's turn it is (human or computer)
    bool humansTurn_;
};


inline void TakeAwayState::initialise(GameMode mode, int pennies) {
    gameMode_ = mode;
    initialPennies_ = currentPennies_ = pennies;
    humansTurn_ = true;
}

inline GameMode TakeAwayState::getGameMode() const {
    return gameMode_;
}

inline int TakeAwayState::getInitialPennies() const {
    return initialPennies_;
}

inline int TakeAwayState::getCurrentPennies() const {
    return currentPennies_;
}

inline bool TakeAwayState::isHumansTurn() const {
    return humansTurn_;
}

inline void TakeAwayState::setCurrentPennies(int pennies) {
    currentPennies_ = pennies;
}

inline void TakeAwayState::setHumansTurn(bool humansTurn) {
    humansTurn_ = humansTurn;
}

#endif // TAKEAWAYSTATE_H

