// 
// Author:      Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#include "CrossoutGame.h"

int main(int argc, char* argv[]) {
    CrossoutGame game;
    return game.run(argc, argv);
}
