// 
// Author:      Jedd Haberstro
// Repository:  https://bitbucket.org/jhaberstro/cs4-project/overview
//              git clone https://jhaberstro@bitbucket.org/jhaberstro/cs4-project.git
//

#include "NimGame.h"

int main(int argc, char* argv[]) {
    NimGame game;
    return game.run(argc, argv);
}
