solution "Project"
    common_files = { "Game.h", "Game.cpp", "State.h" }
    configurations { "Debug" }
    location "build"
    project "takeaway"
        targetdir "build"
        kind "ConsoleApp"
        language "C++"
        files { common_files, "TakeAway**", "takeaway.cpp" }
        configuration "Debug"
            flags { "Symbols" }
    project "nim"
        targetdir "build"
        kind "ConsoleApp"
        language "C++"
        files { common_files, "Nim**", "nim.cpp" }
        configuration "Debug"
            flags { "Symbols" }
    project "othello"
        targetdir "build"
        kind "ConsoleApp"
        language "C++"
        files { common_files, "Othello**", "othello.cpp" }
        configuration "Debug"
            flags { "Symbols" }
    project "crossout"
        targetdir "build"
        kind "ConsoleApp"
        language "C++"
        files { common_files, "Crossout**", "crossout.cpp" }
        configuration "Debug"
            flags { "Symbols" }
